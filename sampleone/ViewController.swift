//
//  ViewController.swift
//  sampleone
//
//  Created by Thisura Dodangoda on 8/15/20.
//  Copyright © 2020 Thisura Dodangoda. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet private weak var container: UIView!
    @IBOutlet private weak var round: UIView!
    @IBOutlet private weak var textView: UITextView!
    
    private let showDetailSegue: String = "showDetail"
    private var isFirstTime: Bool = true
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setup()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == showDetailSegue{
            let dest = segue.destination as! SecondController
            // dest.modalPresentationStyle = .custom
            dest.transitioningDelegate = self
        }
    }

    private func setup(){
        guard isFirstTime else { return }
        isFirstTime = false
        
        container.clipsToBounds = true
        container.layer.cornerRadius = 12.0
        
        round.clipsToBounds = true
        round.layer.cornerRadius = round.frame.height / 2.0
    }
    
    @IBAction private func triggerPressed(){
        performSegue(withIdentifier: showDetailSegue, sender: nil)
    }
    
}

extension ViewController: UIViewControllerTransitioningDelegate{
    
    /*
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        
        return CustomPresentationController.init(
            presentedViewController: presented,
            presenting: presenting)
        
    }*/
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        let animator = CustomAnimationController()
        animator.sourceFrame = container.frame
        return animator
        
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        let animator = CustomAnimationController()
        animator.targetFrame = container.frame
        return animator
        
    }
    
}
