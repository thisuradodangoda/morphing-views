//
//  SecondController.swift
//  sampleone
//
//  Created by Thisura Dodangoda on 8/15/20.
//  Copyright © 2020 Thisura Dodangoda. All rights reserved.
//

import UIKit

class SecondController: UIViewController {
    
    @IBOutlet private weak var container: UIView!
    @IBOutlet private weak var round: UIView!
    @IBOutlet private weak var backBtn: UIButton!
    @IBOutlet private weak var textView: UITextView!
    @IBOutlet private weak var headerHeight: NSLayoutConstraint!
    
    private let headerHeightStart: CGFloat = 80.0
    private let headerHeightEnd: CGFloat = 300.0
    
    private var isFirstTime: Bool = true
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
        setup()
        headerHeight.constant = headerHeightStart
        view.layoutIfNeeded()
        headerAnimation(directionIn: true)
    }
    
    private func headerAnimation(directionIn: Bool){
        let targetHeight = directionIn ? headerHeightEnd : headerHeightStart
        
        if directionIn{
            backBtn.alpha = 0.0
        }
        
        transitionCoordinator!.animate(alongsideTransition: { [weak self] (_) in
            
            guard let s = self else { return }
            
            if directionIn{
                // s.headerHeight.constant = s.headerHeightStart
                // s.view.layoutIfNeeded()
                s.backBtn.alpha = 1.0
            }
            else{
                s.backBtn.alpha = 0.0
            }
            
            s.headerHeight?.constant = targetHeight
            
            s.view?.layoutIfNeeded()
            
        }) { (_) in
            // print("SecondController animation:", !context.isCancelled)
        }
        
        /*UIView.animate(
            withDuration: 0.72,
        delay: 0.0,
        options: [.curveEaseInOut],
        animations: { [weak self] in
            self?.view?.layoutIfNeeded()
        }) { (completed) in
            print("SecondController animation:", completed)
        }*/
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }

    private func setup(){
        guard isFirstTime else { return }
        isFirstTime = false
        
        container.clipsToBounds = true
        container.layer.cornerRadius = 12.0
        
        round.clipsToBounds = true
        round.layer.cornerRadius = round.frame.height / 2.0
        
        backBtn.clipsToBounds = true
        backBtn.layer.cornerRadius = backBtn.frame.height / 2.0
    }
    
    @IBAction private func roundClicked(){
        print("Round View Clicked")
    }
    
    @IBAction private func backBtnPressed(){
        // navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
        transitionCoordinator!.animate(alongsideTransition: { [weak self] (_) in
            self?.headerAnimation(directionIn: false)
        }, completion: nil)
    }

}
