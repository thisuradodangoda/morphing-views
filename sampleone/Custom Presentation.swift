//
//  Custom Presentation.swift
//  sampleone
//
//  Created by Thisura Dodangoda on 8/15/20.
//  Copyright © 2020 Thisura Dodangoda. All rights reserved.
//

import UIKit

class AnimationConstants{
    
    // MARK: Singleton Stuff
    private static var _instance: AnimationConstants?
    static var instance: AnimationConstants{
        get{
            if _instance == nil{
                _instance = AnimationConstants()
            }
            return _instance!
        }
    }
    private init(){}
    
    // MARK: Constants
    
    let animDuration: TimeInterval = 0.78
    let animDelay: TimeInterval = 0.0
    
}

/*
class CustomPresentationController: UIPresentationController{
    
    override func presentationTransitionWillBegin() {
        print("CustomPresentation willBegin")
        super.presentationTransitionWillBegin()
    }
    
    override func presentationTransitionDidEnd(_ completed: Bool) {
        print("CustomPresentation didEnd completed:", completed)
        super.presentationTransitionDidEnd(completed)
    }
    
}*/

@objc protocol CustomAnimationProtocol{
    @objc optional func beforeInAnimation()
    @objc optional func duringInAnimation()
    @objc optional func afterInAnimation()
    
    @objc optional func beforeOutAnimation()
    @objc optional func duringOutAnimation()
    @objc optional func afterOutAnimation()
}

class CustomAnimationController: NSObject, UIViewControllerAnimatedTransitioning{
    
    private enum AnimationState{
        case before, during, after
    }
    
    var sourceFrame: CGRect!
    var targetFrame: CGRect!
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        
        return AnimationConstants.instance.animDuration + AnimationConstants.instance.animDelay
    }
    
    private func notify(
    _ this: UITransitionContextViewControllerKey,
    _ context: UIViewControllerContextTransitioning,
    _ isIn: Bool,
    _ state: AnimationState){
        
        let rawVC = context.viewController(forKey: this)
        var vc: CustomAnimationProtocol
        
        if let nav = rawVC as? UINavigationController{
            guard let _vc = nav.topViewController as? CustomAnimationProtocol else{
                return
            }
            vc = _vc
        }
        else{
            guard let _vc = rawVC as? CustomAnimationProtocol else{
                return
            }
            vc = _vc
        }
        
        switch(isIn, state){
        case (true, .before): vc.beforeInAnimation?()
        case (true, .during): vc.duringInAnimation?()
        case (true, .after): vc.afterInAnimation?()
        case (false, .before): vc.beforeOutAnimation?()
        case (false, .during): vc.duringOutAnimation?()
        case (false, .after): vc.afterInAnimation?()
        }
        
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        let delay = AnimationConstants.instance.animDelay
        let duration = AnimationConstants.instance.animDuration
        
        // Pushing
        if let dest = transitionContext.view(forKey: .to){
            
            notify(.to, transitionContext, true, .before)
            notify(.from, transitionContext, true, .before)
            
            dest.frame = sourceFrame
            dest.layoutIfNeeded()
            transitionContext.containerView.addSubview(dest)
            
             // Successful implementation
             // Commented to test with Constraints, as
             // implemented above.
             
            UIView.animate(withDuration: duration, animations: { [weak self] in
                
                dest.frame = transitionContext.containerView.frame
                self?.notify(.to, transitionContext, true, .during)
                self?.notify(.from, transitionContext, true, .during)
                transitionContext.containerView.layoutIfNeeded()
                
            }) { [weak self] (completed) in
                self?.notify(.to, transitionContext, true, .after)
                self?.notify(.from, transitionContext, true, .after)
                
                transitionContext.completeTransition(true)
            }
            
            /*
            dest.frame = sourceFrame
            dest.layoutIfNeeded()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                UIView.animate(withDuration: duration, animations: {
                    dest.frame = transitionContext.containerView.frame
                    dest.layoutIfNeeded()
                }) { (completed) in
                    transitionContext.completeTransition(true)
                }
            }*/
            
        }
        
        // Popping
        if let dest = transitionContext.view(forKey: .from){
            
            notify(.to, transitionContext, false, .before)
            notify(.from, transitionContext, false, .before)
            
            UIView.animate(withDuration: duration, animations: { [weak self] in
                guard let s = self else { return }
                dest.frame = s.targetFrame
                dest.safeAreaLayoutGuide.constraintsAffectingLayout(for: .horizontal).forEach {
                    guard let id = $0.identifier else { return }
                    if id.contains("UIViewSafeAreaLayoutGuide"){
                        $0.constant = 0.0
                    }
                }
                
                self?.notify(.to, transitionContext, false, .during)
                self?.notify(.from, transitionContext, false, .during)
                
                /*
                var adjustedBounds = dest.subviews[0].bounds
                adjustedBounds.size.width += adjustedBounds.origin.x
                adjustedBounds.origin.x = 0
                dest.subviews[0].bounds = adjustedBounds*/
                transitionContext.containerView.layoutIfNeeded()
            }) { [weak self] (completed) in
                transitionContext.completeTransition(true)
                self?.notify(.to, transitionContext, false, .after)
                self?.notify(.from, transitionContext, false, .after)
            }
            
        }
        
    }
    
}
