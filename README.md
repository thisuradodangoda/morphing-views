# morphing-views

Morphing UIView animations.

Utilizes frame animations and not Auto Layout changes. Approaches using AutoLayout gave unpredictable results. Frames are more concise when using for positioning container 

### Example ###

![Recording](https://gitlab.com/thisuradodangoda/morphing-views/-/raw/9b25dd6fd2f9803ca20f421cf282009456254802/recording.gif?inline=false)
